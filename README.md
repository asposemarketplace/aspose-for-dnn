## What is DotNetNuke?
DotNetNuke also called DNN is an open source and free CMS (content management system) based on the .NET framework. DNN allows you to create unlimited number of websites, either as root websites or for portals in a parent-child configuration. It is enriched with common CMS features like dynamic page creation, Skinning, Searching and custom module developement. Here are the projects we have created for this popular open source platform


1. ##[Aspose DotNetNuke Module Development Template](https://bitbucket.org/asposemarketplace/aspose-for-dnn/wiki/Aspose%20for%20.NET%20DNN%20Module%20Development%20Template)
Aspose for .NET DNN Module Development Template provides a quick and easy to use Module development template for DNN 7+ with ability to automatically download selected Aspose components and seamlessly adding them to your DNN module project on the fly ... [ Continue reading →](https://bitbucket.org/asposemarketplace/aspose-for-dnn/wiki/Aspose%20for%20.NET%20DNN%20Module%20Development%20Template)


2. ##[Aspose .NET Word Processing and PDF Document Exporter for DNN Module](https://bitbucket.org/asposemarketplace/aspose-for-dnn/wiki/Aspose%20.NET%20Word%20Processing%20and%20PDF%20Document%20Exporter%20for%20DNN%20Module)
Aspose .NET Word Processing and PDF Document Exporter for DNN allow users to export online content into Word Processing or PDF document using Aspose.Words ... [ Continue reading →](https://bitbucket.org/asposemarketplace/aspose-for-dnn/wiki/Aspose%20.NET%20Word%20Processing%20and%20PDF%20Document%20Exporter%20for%20DNN%20Module)



3. ##[Aspose .NET Word Import for DNN Module](https://bitbucket.org/asposemarketplace/aspose-for-dnn/wiki/Aspose%20.NET%20Word%20Import%20for%20DNN%20Module)
Aspose .NET Word Import for DNN Module allows developers to get/read contents of Word document without requiring any other software such as Microsoft Word or OpenOffice ... [ Continue reading →](https://bitbucket.org/asposemarketplace/aspose-for-dnn/wiki/Aspose%20.NET%20Word%20Import%20for%20DNN%20Module)



4. ##[Aspose .NET PDF Import for DNN Module](https://bitbucket.org/asposemarketplace/aspose-for-dnn/wiki/Aspose%20.NET%20PDF%20Import%20for%20DNN%20Module)
Aspose .NET PDF Import for DNN Module allows developers to get/read contents of PDF document without requiring any other software such as Adobe Acrobat or PDF reader ... [ Continue reading →](https://bitbucket.org/asposemarketplace/aspose-for-dnn/wiki/Aspose%20.NET%20PDF%20Import%20for%20DNN%20Module)



5. ##[Aspose .NET Exchange Sync for DNN](https://bitbucket.org/asposemarketplace/aspose-for-dnn/wiki/DNN%20Exchange%20Sync%20%E2%80%93%202%20Way%20Link%20between%20DNN%20Users%20and%20Microsoft%20Exchange%20Server%20Contacts)
DNN Exchange Sync is an open source module from Aspose that links your DNN users to Microsoft Exchange Server contacts without requiring any other software ... [ Continue reading →](https://bitbucket.org/asposemarketplace/aspose-for-dnn/wiki/DNN%20Exchange%20Sync%20%E2%80%93%202%20Way%20Link%20between%20DNN%20Users%20and%20Microsoft%20Exchange%20Server%20Contacts)


6. ##[Aspose .NET Gmail Sync for DNN](https://bitbucket.org/asposemarketplace/aspose-for-dnn/wiki/Sync%20DNN%20Users%20with%20Google%20Contacts%20using%20Aspose%20.NET%20Gmail%20Sync%20for%20DNN)
Aspose .NET Gmail Sync for DNN is an open source module from Aspose that links your DNN users to Googleor Gmail contacts without requiring any other software... [ Continue reading →](https://bitbucket.org/asposemarketplace/aspose-for-dnn/wiki/Sync%20DNN%20Users%20with%20Google%20Contacts%20using%20Aspose%20.NET%20Gmail%20Sync%20for%20DNN)


## What is the use of Aspose .NET Products?

[Aspose](http://www.aspose.com) are file format experts and provide APIs and components for various file formats including MS Office, OpenOffice, PDF and Image formats. These APIs are available on a number of development platforms including .NET
 frameworks &ndash; the .NET frameworks starting from version 2.0 are supported. If you are a .NET developer, you can use Aspose’s native .NET APIs in your .NET applications to process various file formats in just a few lines of codes. All the Aspose
 APIs don’t have any dependency over any other engine. For example, you don’t need to have MS Office installed on the server to process MS Office files. Below is a list of products we support for .NET developers:


## Aspose.Cells for .NET

[![Aspose.Cells for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_cells-for-net.jpg)](http://www.aspose.com/.net/excel-component.aspx)

Using these APIs, the .NET developers can perform simple and complex operations over various spreadsheet formats including MS Excel and OpenOffice spreadsheets. The APIs also provide conversion and rendering
 features for these file formats.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/excel-component.aspx)


## Aspose.Words for .NET

[![Aspose.Words for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_words-for-net.jpg)](http://www.aspose.com/.net/word-component.aspx)

Using these APIs, the .NET developers can perform simple and complex operations over various word processing formats including MS Word and OpenOffice documents. The APIs also provide conversion and rendering
 features for these file formats.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/word-component.aspx)


## Aspose.Pdf for .NET

[![Aspose.PDF for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_pdf-for-net.jpg)](http://www.aspose.com/.net/pdf-component.aspx)

Using these APIs, the .NET developers can perform simple and complex operations over PDF files. The APIs also provide conversion and rendering features for these file formats.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/pdf-component.aspx)


## Aspose.Slides for .NET

[![Aspose.Slides for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_slides-for-net.jpg)](http://www.aspose.com/.net/powerpoint-component.aspx)

Using these APIs, the .NET developers can perform simple and complex operations over various presentation formats including MS PowerPoint and OpenOffice presentations. The APIs also provide conversion and rendering features for these file formats.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/powerpoint-component.aspx)


## Aspose.BarCode for .NET

[![Aspose.BarCode for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_barcode-for-net.jpg)](http://www.aspose.com/.net/barcode-component.aspx)

Using these APIs, the .NET developers can generate and recognize a variety of barcode symbologies. Create barcode applications, or add barcodes to documents using these APIs.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/barcode-component.aspx)


## Aspose.Tasks for .NET

[![Aspose.Tasks for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_tasks-for-net.jpg)](http://www.aspose.com/.net/project-management-component.aspx)

Using these APIs, the .NET developers can create, read, manipulate, convert and save Microsoft Project files. The APIs also provide conversion and rendering features for MS Project file formats.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/project-management-component.aspx)


## Aspose.Email for .NET

[![Aspose.Email for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_email-for-net.jpg)](http://www.aspose.com/.net/email-component.aspx)

Using these APIs, the .NET developers can perform simple and complex operations over various email formats including MS Outlook email formats. The APIs also provide conversion and rendering features for these file formats.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/email-component.aspx)


## Aspose.Diagram for .NET

[![Aspose.Diagram for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_diagram-for-net.jpg)](http://www.aspose.com/.net/diagram-component.aspx)

Using these APIs, the .NET developers can work with Microsoft Visio drawing files. The APIs enable developers to quickly create .NET applications for manipulating and converting Microsoft Visio drawing files.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/diagram-component.aspx)[](http://www.aspose.com/demos/.net-components/aspose.diagram/default.aspx)



## Aspose.OCR for .NET

[![Aspose.OCR for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_ocr-for-net.jpg)](http://www.aspose.com/.net/ocr-component.aspx)

Using these APIs, the .NET developers can perform OCR operations over images. The APIs hide all the complexities involved with OCR and developers only need to write few lines of codes to call public interfaces exposed by the API.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/ocr-component.aspx)[](http://www.aspose.com/demos/.net-components/aspose.ocr/default.aspx)


## Aspose.Imaging for .NET


[![Aspose.Imaging for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_imaging-for-net.jpg)
](http://www.aspose.com/.net/imaging-component.aspx)

Using these APIs, the .NET developers can create, open, manipulate and save images of various formats.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/imaging-component.aspx)[](http://www.aspose.com/demos/.net-components/aspose.imaging/default.aspx)